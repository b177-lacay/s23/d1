// select a database
use <database name>

// when creating a new database via the command line, the use command can be entered with a name of a database that does not yet exist. Once a new record is inserted into that database, the database will be created

// database = filling cabinet
// collection = drawer
// document/record = folder inside a drawer
// sub-documents (optional) = other files
// fields = file content
//select a database
use <database name>



/*
e.g. document with no sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}
e.g. document with sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		address: {
			street: "123 Street St",
			city: "Makati"
			country: "Philippines"
		}
	}
*/


db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age : 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript","Python"],
	department: "none"

})

// if inserting/creating a new document within a collection that does not yet exist, MongoDB will automatically create that collection

// insert many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"

	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}

	])

// Finding documents ( Read)

// retrieve a list of ALL users
db.users.find()

// find a specific user

db.users.find({ firstName: "Stephen"})
db.users.findOne({ firstName: "Stephen"})

// findy documents with multiple parameters/conditions
db.users.find({lastName: "Armstrong", age : 82})

// Update/edit Documents

// create a new document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
			phone: "123456789",
			email: "testg@mail.com"
		},
		courses: [],
		department: "none"
	})

// update one always needs to be provided two objects. The first object specofoes WHICH document to update.The second object contains the $set opearator, and inside the $set operator are the specific fields to be updated 
db.users.updateOne(
	{ _id: ObjectId("62876c726fc7c5763900abfc")},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
					phone: "123456789",
					email: "billgatesg@mail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations"
		}

	}

	)

// updating multiple documents
db.users.updateMany(
	{department: "none"},
	{
		$set : {
			department : "HR"
		}
	}

	)

// Deleting a single document

// creating a document to delete
db.users.insert({
	firstName: "Test",
	})
// deleting a single document
db.users.deleteOne({
	firstName: "Test"
})

// deleting many records
// Be cvareful when using deleteMany. If no search cristeria are provided, it will delete ALL documents within the given collection

// create another user to delete
db.users.insert({
	firstName: "Bill"
})

db.users.deleteMany({
	firstName: "Bill"
})